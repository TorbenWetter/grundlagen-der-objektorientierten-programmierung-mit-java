package org.iu.dsgopj102101.uebungsprojekt;

import org.iu.dsgopj102101.uebungsprojekt.buchfuehrung.Buchung;
import org.iu.dsgopj102101.uebungsprojekt.buchfuehrung.Konto;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Buchfuehrung {

    private static final String FILE_PATH = "src/org/iu/dsgopj102101/uebungsprojekt/buchfuehrung/data.txt";

    private static List<Konto> konten = new LinkedList<>();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean beenden = false;
        while (!beenden) {
            System.out.println("--------------------");
            System.out.println("Was möchten Sie tun?");
            System.out.print("Konto anlegen (1), Buchung vornehmen (2), Kontostand anzeigen (3), Speichern (4), Laden (5) oder Beenden (6) ");
            String auswahlAktion = scanner.nextLine().trim();
            if (auswahlAktion.equals("1")) {
                System.out.print("Wie soll das Konto heißen? ");
                String kontoName = scanner.nextLine().trim();
                if (konten.stream().anyMatch(konto -> konto.getName().equals(kontoName))) {
                    System.out.println("Ein Konto mit diesem Namen existiert bereits.");
                } else {
                    kontoAnlegen(kontoName);
                    System.out.println("Das Konto wurde erfolgreich angelegt.");
                }
            } else if (auswahlAktion.equals("2")) {
                if (konten.isEmpty()) {
                    System.out.println("Es existiert kein Konto.");
                    continue;
                }

                System.out.print("Auf welches Konto soll gebucht werden? ");
                String kontoName = scanner.nextLine().trim();
                if (konten.stream().anyMatch(konto -> konto.getName().equals(kontoName))) {
                    System.out.print("Wie soll der Buchungstext lauten? ");
                    String buchungsText = scanner.nextLine().trim();
                    System.out.print("Wie lautet der Betrag? ");
                    double buchungsBetrag = scanner.nextDouble();
                    scanner.nextLine(); // Workaround to ignore \n after double
                    buchungVornehmen(kontoName, buchungsText, buchungsBetrag);
                    System.out.println("Die Buchung wurde erfolgreich vorgenommen.");
                } else {
                    System.out.println("Es existiert kein Konto mit diesem Namen.");
                }
            } else if (auswahlAktion.equals("3")) {
                if (konten.isEmpty()) {
                    System.out.println("Es existiert kein Konto.");
                    continue;
                }

                System.out.print("Von welchem Konto möchten Sie den Kontostand anzeigen? ");
                String kontoName = scanner.nextLine().trim();
                if (konten.stream().anyMatch(konto -> konto.getName().equals(kontoName))) {
                    double kontostand = kontostandErmitteln(kontoName);
                    System.out.printf("Kontostand: %.2f\n", kontostand);
                } else {
                    System.out.println("Es existiert kein Konto mit diesem Namen.");
                }
            } else if (auswahlAktion.equals("4")) {
                try {
                    speichern();
                    System.out.println("Alle Daten wurden erfolgreich gespeichert.");
                } catch (IOException ex) {
                    System.out.println("Es gab einen Fehler beim Speichern der Datei.");
                }
            } else if (auswahlAktion.equals("5")) {
                try {
                    laden();
                    System.out.println("Alle Daten wurden erfolgreich geladen.");
                } catch (IOException ex) {
                    System.out.println("Es gab einen Fehler beim Laden der Datei.");
                }
            } else if (auswahlAktion.equals("6")) {
                beenden = true;
                System.out.println("Das Programm wird beendet.");
            } else {
                System.out.println("Ungültige Auswahl!");
            }
        }
    }

    private static void kontoAnlegen(String kontoName) {
        Konto konto = new Konto(kontoName);
        konten.add(konto);
    }

    private static void buchungVornehmen(String kontoName, String text, double betrag) {
        Konto buchungsKonto = konten.stream().filter(konto -> konto.getName().equals(kontoName)).findFirst().get();
        Buchung buchung = new Buchung(betrag, text);
        buchungsKonto.addBuchung(buchung);
    }

    private static double kontostandErmitteln(String kontoName) {
        Konto angefragtesKonto = konten.stream().filter(konto -> konto.getName().equals(kontoName)).findFirst().get();
        return angefragtesKonto.kontostandBerechnen();
    }

    private static void speichern() throws IOException {
        PrintWriter out = new PrintWriter(FILE_PATH);
        out.println(konten.size());
        for (Konto konto : konten) {
            out.println(konto.getName());
            out.println(konto.getBuchungen().size());
            for (Buchung buchung : konto.getBuchungen()) {
                out.println(String.valueOf(buchung.getDatum().getTime()));
                out.println(String.valueOf(buchung.getBetrag()));
                out.println(buchung.getText());
            }
        }
        out.close();
    }

    private static void laden() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(FILE_PATH));

        int anzahlKonten = Integer.parseInt(br.readLine());
        for (int i = 0; i < anzahlKonten; i++) {
            String kontoName = br.readLine();
            Konto konto = new Konto(kontoName);

            int anzahlBuchungen = Integer.parseInt(br.readLine());
            for (int j = 0; j < anzahlBuchungen; j++) {
                Date buchungsDatum = new Date(Long.parseLong(br.readLine()));
                double buchungsBetrag = Double.parseDouble(br.readLine());
                String buchungsText = br.readLine();

                Buchung buchung = Buchung.mitDatumErstellen(buchungsDatum, buchungsBetrag, buchungsText);
                konto.addBuchung(buchung);
            }

            konten.add(konto);
        }

        br.close();
    }
}
