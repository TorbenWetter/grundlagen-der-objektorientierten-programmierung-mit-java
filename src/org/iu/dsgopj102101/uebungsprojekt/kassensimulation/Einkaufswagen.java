package org.iu.dsgopj102101.uebungsprojekt.kassensimulation;

public class Einkaufswagen implements Comparable<Einkaufswagen> {

    private int numArtikel;
    private long wartezeit;

    public Einkaufswagen(int numArtikel) {
        this.numArtikel = numArtikel;
    }

    public int getNumArtikel() {
        return numArtikel;
    }

    public void setNumArtikel(int numArtikel) {
        this.numArtikel = numArtikel;
    }

    public long getWartezeit() {
        return wartezeit;
    }

    public void setWartezeit(long wartezeit) {
        this.wartezeit = wartezeit;
    }

    public String toString() {
        return "" + numArtikel;
    }

    @Override
    public int compareTo(Einkaufswagen o) {
        return numArtikel - o.getNumArtikel();
    }

}
