package org.iu.dsgopj102101.uebungsprojekt.kassensimulation;

import java.util.List;

public class FifoKassierStrategie implements IKassierStrategie {

    private double mittlereWartezeit;

    @Override
    public String name() {
        return "First In First Out";
    }

    @Override
    public void start(int numKassen, int numEinkaufswaegen, int numMaxArtikel) {
        int wartezeit = 0;
        for (int i = 0; i < numKassen; i++) {
            Kasse kasse = new Kasse();

            List<Einkaufswagen> schlange = EinkaufswagenFactory.generate(1, numMaxArtikel, numEinkaufswaegen);
            for (Einkaufswagen einkaufswagen : schlange) {
                kasse.kassieren(einkaufswagen);
                wartezeit += einkaufswagen.getWartezeit();
            }
        }
        this.mittlereWartezeit = Double.valueOf(wartezeit) / numEinkaufswaegen / numKassen / 60;
    }

    @Override
    public double getMittlereWartezeit() {
        return this.mittlereWartezeit;
    }

}
