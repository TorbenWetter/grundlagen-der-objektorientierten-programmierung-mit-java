package org.iu.dsgopj102101.uebungsprojekt.kassensimulation;

import java.util.Collections;
import java.util.List;

public class SifoKassierStrategie implements IKassierStrategie {

    private double mittlereWartezeit;

    @Override
    public String name() {
        return "Smallest First";
    }

    @Override
    public void start(int numKassen, int numEinkaufswaegen, int numMaxArtikel) {
        int wartezeit = 0;
        for (int i = 0; i < numKassen; i++) {
            Kasse kasse = new Kasse();

            List<Einkaufswagen> schlange = EinkaufswagenFactory.generate(1, numMaxArtikel, numEinkaufswaegen);
            Collections.sort(schlange);
            for (Einkaufswagen einkaufswagen : schlange) {
                kasse.kassieren(einkaufswagen);
                wartezeit += einkaufswagen.getWartezeit();
            }
        }
        this.mittlereWartezeit = Double.valueOf(wartezeit) / numEinkaufswaegen / numKassen / 60;
    }

    @Override
    public double getMittlereWartezeit() {
        return this.mittlereWartezeit;
    }

}
