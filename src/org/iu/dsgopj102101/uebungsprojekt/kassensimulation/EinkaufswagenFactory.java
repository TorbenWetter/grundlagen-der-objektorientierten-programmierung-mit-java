package org.iu.dsgopj102101.uebungsprojekt.kassensimulation;

import java.util.ArrayList;
import java.util.List;

public class EinkaufswagenFactory {

    private List<Einkaufswagen> einkaufswaegen;

    public EinkaufswagenFactory(int min, int max, int num) {
        this.setEinkaufswaegen(new ArrayList<Einkaufswagen>());
        generateWagen(min, max, num);
    }

    private void generateWagen(int min, int max, int num) {
        for (int i = 0; i < num; i++) {
            int numArtikel = MyRandom.randomInt(min, max);
            Einkaufswagen ew = new Einkaufswagen(numArtikel);
            getEinkaufswaegen().add(ew);
        }
    }

    public static List<Einkaufswagen> generate(int min, int max, int num) {
        EinkaufswagenFactory ewf = new EinkaufswagenFactory(min, max, num);
        return ewf.getEinkaufswaegen();
    }

    public List<Einkaufswagen> getEinkaufswaegen() {
        return this.einkaufswaegen;
    }

    private void setEinkaufswaegen(List<Einkaufswagen> einkaufswaegen) {
        this.einkaufswaegen = einkaufswaegen;
    }
}
