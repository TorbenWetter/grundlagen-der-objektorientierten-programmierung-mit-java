package org.iu.dsgopj102101.uebungsprojekt;

import org.iu.dsgopj102101.uebungsprojekt.weihnachten.Weihnachtsbaumkugel;

public class Weihnachten {

    public static void main(String[] args) {
        Weihnachtsbaumkugel[] weihnachtsbaumkugeln = new Weihnachtsbaumkugel[]{new Weihnachtsbaumkugel(5),
                new Weihnachtsbaumkugel(7.5)};
        for (Weihnachtsbaumkugel weihnachtsbaumkugel : weihnachtsbaumkugeln) {
            double durchmesser = weihnachtsbaumkugel.getDurchmesser();
            double volumen = weihnachtsbaumkugel.berechneVolumen();
            double oberflaeche = weihnachtsbaumkugel.berechneOberflaeche();
            System.out.println("Durchmesser: " + durchmesser + "cm, Volumen: " + volumen + "cmˆ3, Oberfläche: "
                    + oberflaeche + "cmˆ2");
        }
    }

}
