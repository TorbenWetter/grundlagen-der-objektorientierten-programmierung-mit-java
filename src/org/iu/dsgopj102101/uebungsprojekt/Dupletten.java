package org.iu.dsgopj102101.uebungsprojekt;

import org.iu.dsgopj102101.uebungsprojekt.dupletten.Person;

import java.util.LinkedList;
import java.util.List;

public class Dupletten {

    public static void main(String[] args) {
        // Create an Array of Persons which contains duplicates (different objects containing the same attributes)
        Person[] registeredPersons = new Person[]{
                new Person("Jessica", "Faber", "13.8.1954", "2650 Jarvisville Road; Westbury, New York(NY), 11590", "IIkeezoII"),
                new Person("Jessica", "Faber", "13.8.1954", "2650 Jarvisville Road; Westbury, New York(NY), 11590", "IIkeezoII"),
                new Person("Judith", "McCall", "9.3.1998", "3434 John Daniel Drive; Cuba, Missouri(MO), 65453", "jim_scout"),
                new Person("Judith", "McCall", "9.3.1998", "3434 John Daniel Drive; Cuba, Missouri(MO), 65453", "jim_scout"),
                new Person("Judith", "McCall", "9.3.1998", "3434 John Daniel Drive; Cuba, Missouri(MO), 65453", "jim_scout"),
                new Person("Judith", "McCall", "9.3.1998", "3434 John Daniel Drive; Cuba, Missouri(MO), 65453", "jim_scout"),
                new Person("Eric", "Kelley", "5.8.1971", "3382 Stonepot Road; Clinton, New Jersey(NJ), 08809", "Xman_360"),
                new Person("Eric", "Kelley", "5.8.1971", "3382 Stonepot Road; Clinton, New Jersey(NJ), 08809", "Xman_360"),
                new Person("Eric", "Kelley", "5.8.1971", "3382 Stonepot Road; Clinton, New Jersey(NJ), 08809", "Xman_360"),
                new Person("Fred", "Schultz", "12.7.1976", "1482 Lake Floyd Circle; Hockessin, Delaware(DE), 19707", "Stripyneko483")
        };
        System.out.println("Anzahl Personen: " + registeredPersons.length);

        // Only add unique Persons (no duplicates) to list
        List<Person> uniquePersons = new LinkedList<>();
        for (Person registeredPerson : registeredPersons) {
            // True if uniquePersons contains at least one element e such that Objects.equals(registeredPerson, e)
            if (!uniquePersons.contains(registeredPerson)) {
                // This person is new and can therefore be added to the uniquePersons list
                uniquePersons.add(registeredPerson);
            }
        }
        System.out.println("Anzahl einzigartiger Personen: " + uniquePersons.size());
    }
}
