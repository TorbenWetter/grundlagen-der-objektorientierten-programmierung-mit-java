package org.iu.dsgopj102101.uebungsprojekt.uml;

public class Auto {

    private String kennzeichen;
    private int ps;

    public void drive() {
        System.out.println("Auto " + getKennzeichen() + " fährt");
    }

    public void setKennzeichen(String kennzeichen) {
        this.kennzeichen = kennzeichen;
    }

    public String getKennzeichen() {
        return kennzeichen;
    }

    public void setPs(int ps) {
        this.ps = ps;
    }

    public int getPs() {
        return ps;
    }

}
