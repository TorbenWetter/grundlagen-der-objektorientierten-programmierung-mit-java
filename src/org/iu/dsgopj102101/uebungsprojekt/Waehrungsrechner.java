package org.iu.dsgopj102101.uebungsprojekt;

import org.iu.dsgopj102101.uebungsprojekt.iotools.IOTools;
import org.iu.dsgopj102101.uebungsprojekt.waehrungsrechner.Geld;
import org.iu.dsgopj102101.uebungsprojekt.waehrungsrechner.Waehrung;

public class Waehrungsrechner {

    public static void main(String[] args) {
        while (true) {
            System.out.println("--------------------------------");
            try {
                String ausgangsWaehrungCode = IOTools.readString("ISO-Code Ausgangswährung: ");
                Waehrung ausgangsWaehrung = Waehrung.getWaehrung(ausgangsWaehrungCode);
                System.out.println("Konvertiere von: " + ausgangsWaehrung);

                String zielWaehrungCode = IOTools.readString("ISO-Code Zielwährung: ");
                Waehrung zielWaehrung = Waehrung.getWaehrung(zielWaehrungCode);
                System.out.println("In: " + zielWaehrung);

                double betrag = IOTools.readDouble("Betrag: ");
                Geld ausgangsVermoegen = new Geld(ausgangsWaehrung, betrag);
                System.out.println("Ausgangsvermögen: " + ausgangsVermoegen);

                Geld zielVermoegen = Geld.konvertiereZu(ausgangsVermoegen, zielWaehrung);
                System.out.println("Zielvermögen: " + zielVermoegen);
            } catch (IllegalArgumentException ex) {
                ex.printStackTrace();
            }
        }
    }
}
