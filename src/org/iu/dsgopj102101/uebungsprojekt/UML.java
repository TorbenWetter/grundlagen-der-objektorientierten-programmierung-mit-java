package org.iu.dsgopj102101.uebungsprojekt;

import org.iu.dsgopj102101.uebungsprojekt.iotools.IOTools;
import org.iu.dsgopj102101.uebungsprojekt.uml.Auto;
import org.iu.dsgopj102101.uebungsprojekt.uml.Person;

public class UML {

    public static void main(String[] args) {
        // Code auf Basis der IOTools von Scheffler (Scheffler, 2018)
        String vorname = IOTools.readString("Vorname: ");
        String nachname = IOTools.readString("Nachname: ");
        Person p = new Person(vorname, nachname);
        String kennzeichen = IOTools.readLine("Kennzeichen: ");
        Auto a = new Auto();
        a.setKennzeichen(kennzeichen);
        int ps = IOTools.readInteger("PS des Autos: ");
        a.setPs(ps);
        String aktion = "";
        while (true) {
            aktion = IOTools.readString("Was möchten Sie tun? (fahren / anhalten)");
            if (aktion.toLowerCase().equals("fahren")) {
                p.fahre(a);
            }
            if (aktion.toLowerCase().equals("anhalten")) {
                break;
            }
        }
    }

}
