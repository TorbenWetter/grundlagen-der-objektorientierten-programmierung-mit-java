package org.iu.dsgopj102101.uebungsprojekt.onlineshop;

public class Artikel {

    private String name;
    private Verkaeufer verkaeufer;

    public Artikel(String name, Verkaeufer verkaeufer) {
        this.name = name;
        this.verkaeufer = verkaeufer;
    }

    public String getName() {
        return name;
    }

    public Verkaeufer getVerkaeufer() {
        return verkaeufer;
    }

}
