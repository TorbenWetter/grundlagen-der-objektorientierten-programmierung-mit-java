package org.iu.dsgopj102101.uebungsprojekt.onlineshop;

import java.util.Date;

public class Bestellung {

    private Date date;
    private Artikel artikel;
    private int menge;
    private Kunde kunde;

    public Bestellung(Artikel artikel, int menge, Kunde kunde) {
        this.date = new Date();
        this.artikel = artikel;
        this.menge = menge;
        this.kunde = kunde;
    }

    public Date getDate() {
        return date;
    }

    public Artikel getArtikel() {
        return artikel;
    }

    public int getMenge() {
        return menge;
    }

    public Kunde getKunde() {
        return kunde;
    }

}
