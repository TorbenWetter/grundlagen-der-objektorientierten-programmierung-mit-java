package org.iu.dsgopj102101.uebungsprojekt.onlineshop;

public class Kunde {

    private String name;
    private String email;

    public Kunde(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public Bestellung bestellen(Artikel artikel, int menge) {
        return new Bestellung(artikel, menge, this);
    }

}
