package org.iu.dsgopj102101.uebungsprojekt.klausur.protokollierung;

public class Person {

    private String vorname;
    private String nachname;
    private String email;
    private String telefonnummer;

    public Person(String vorname, String nachname, String email, String telefonnummer) {
        this.vorname = vorname;
        this.nachname = nachname;
        this.email = email;
        this.telefonnummer = telefonnummer;
    }

    // TODO: Getter und Setter

    public String toString() {
        return vorname + " " + nachname;
    }

}
