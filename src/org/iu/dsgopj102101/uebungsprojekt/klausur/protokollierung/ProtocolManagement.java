package org.iu.dsgopj102101.uebungsprojekt.klausur.protokollierung;

import java.util.ArrayList;
import java.util.List;

public class ProtocolManagement {

    private List<IProtocolEntry> entrys = new ArrayList<IProtocolEntry>();

    public void add(IProtocolEntry e) {
        if (!entrys.contains(e)) {
            entrys.add(e);
        }
    }

    public void showAll() {
        for (int i = 0; i < entrys.size(); i++) {
            IProtocolEntry entry = entrys.get(i);
            System.out.println(entry.getPerson()
                    + "  " + entry.getObject()
                    + "  " + entry.getDateTimeFrom()
                    + " - " + entry.getDateTimeTo());

        }
    }
}
