package org.iu.dsgopj102101.uebungsprojekt.klausur.protokollierung;

import java.util.Date;

public interface IProtocolEntry {

    public Person getPerson();

    public Object getObject();

    public Date getDateTimeFrom();

    public Date getDateTimeTo();

    public String getDescription();

}
