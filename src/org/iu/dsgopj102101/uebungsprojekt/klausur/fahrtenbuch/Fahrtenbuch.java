package org.iu.dsgopj102101.uebungsprojekt.klausur.fahrtenbuch;

import org.iu.dsgopj102101.uebungsprojekt.klausur.protokollierung.Person;
import org.iu.dsgopj102101.uebungsprojekt.klausur.protokollierung.ProtocolManagement;

import java.util.Date;

public class Fahrtenbuch {

    private ProtocolManagement protocolManagement;

    public Fahrtenbuch() {
        this.protocolManagement = new ProtocolManagement();
    }

    public void eintragErgaenzen(Person person, IFahrtenbuchObjekt fahrtenbuchObjekt, Date start, Date ende, String beschreibung) {
        FahrtenbuchEintrag eintrag = new FahrtenbuchEintrag(person, fahrtenbuchObjekt, start, ende, beschreibung);
        protocolManagement.add(eintrag);
    }

    public void eintraegeAuflisten() {
        protocolManagement.showAll();
    }
}
