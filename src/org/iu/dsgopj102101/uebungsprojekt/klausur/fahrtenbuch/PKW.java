package org.iu.dsgopj102101.uebungsprojekt.klausur.fahrtenbuch;

import java.util.Date;

public class PKW extends Fahrzeug {

    private String marke;
    private String modell;
    private String farbe;

    public PKW(String seriennummer, String kennzeichen, Date erstzulassung, float kilometerstand, String marke, String modell, String farbe) {
        super(seriennummer, kennzeichen, erstzulassung, kilometerstand);
        this.marke = marke;
        this.modell = modell;
        this.farbe = farbe;
    }

    // TODO: Getter und Setter

    @Override
    public String toString() {
        return "PKW{" + "seriennummer=" + getSeriennummer() + ", kennzeichen=" + getKennzeichen() + ", erstzulassung=" + getErstzulassung() + ", kilometerstand=" + getKilometerstand() + ", marke=" + marke + ", modell=" + modell + ", farbe=" + farbe + "}";
    }
}
