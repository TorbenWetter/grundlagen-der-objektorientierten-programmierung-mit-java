package org.iu.dsgopj102101.uebungsprojekt.blackjack;

public abstract class Spieler {

    private byte punkte;

    public Spieler() {
        this.punkte = 0;
    }

    public byte getPunkte() {
        return this.punkte;
    }

    public void ziehen(Karte karte) {
        this.punkte += karte.getWert();
    }
}
