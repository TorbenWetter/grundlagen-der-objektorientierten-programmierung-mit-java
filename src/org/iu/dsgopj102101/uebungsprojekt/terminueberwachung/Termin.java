package org.iu.dsgopj102101.uebungsprojekt.terminueberwachung;

import java.util.Date;
import java.util.GregorianCalendar;

public class Termin {

    private final int id;
    private final String betreff;
    private final int mitarbeiterId;
    private final float dauerInStunden;
    private final Date startDatum;
    private final Date endDatum;

    public Termin(int id, String betreff, int mitarbeiterId, float dauerInStunden, Date startDatum, Date endDatum) {
        this.id = id;
        this.betreff = betreff;
        this.mitarbeiterId = mitarbeiterId;
        this.dauerInStunden = dauerInStunden;
        this.startDatum = startDatum;
        this.endDatum = endDatum;
    }

    public static Termin createIfPlausible(int id, String betreff, int mitarbeiterId, float dauerInStunden, Date startDatum, Date endDatum) throws TerminImplausibleException {
        if (startDatum.after(endDatum)) {
            throw new TerminImplausibleException("Startdatum liegt nach Enddatum");
        }

        if (dauerInStunden < 0) {
            throw new TerminImplausibleException("Dauer kann nicht negativ sein");
        }

        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(startDatum);
        gc.add(GregorianCalendar.MILLISECOND, (int) (dauerInStunden * 3600000));
        if (gc.getTime().compareTo(endDatum) > 0) {
            throw new TerminImplausibleException("Dauer ist zu lang");
        } else if (gc.getTime().compareTo(endDatum) < 0) {
            throw new TerminImplausibleException("Dauer ist zu kurz");
        }

        return new Termin(id, betreff, mitarbeiterId, dauerInStunden, startDatum, endDatum);
    }

    @Override
    public String toString() {
        return "Termin{" + "id=" + id + ", betreff=" + betreff + ", mitarbeiterId=" + mitarbeiterId + ", dauerInStunden=" + dauerInStunden + ", startDatum=" + startDatum + ", endDatum=" + endDatum + "}";
    }
}
