package org.iu.dsgopj102101.uebungsprojekt.speicherverwaltung;

import java.lang.reflect.Array;

public class Speicherraum<Objekttyp> {

    public final Objekttyp[][][] objekte;

    @SuppressWarnings("unchecked")
    public Speicherraum(Class<Objekttyp> clazz, int breite, int hoehe, int laenge) {
        Objekttyp[] reihe = (Objekttyp[]) Array.newInstance(clazz, breite);
        Objekttyp[][] quadrat = (Objekttyp[][]) Array.newInstance(clazz, breite, hoehe);
        objekte = (Objekttyp[][][]) Array.newInstance(clazz, breite, hoehe, laenge);
    }

    public void set(Objekttyp objekt, int x, int y, int z) {
        objekte[x][y][z] = objekt;
    }

    public Objekttyp get(int x, int y, int z) {
        return objekte[x][y][z];
    }

    public void remove(int x, int y, int z) {
        set(null, x, y, z);
    }
}
