package org.iu.dsgopj102101.uebungsprojekt.spiel;

public class Buch extends Spielobjekt {

    public Buch(Spieler spieler) {
        super(spieler);
    }

    @Override
    public void benutzen() {
        System.out.println("Spieler " + getSpieler().getName() + " wurde schlauer.");
    }
}
