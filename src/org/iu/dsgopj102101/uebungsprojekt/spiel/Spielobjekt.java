package org.iu.dsgopj102101.uebungsprojekt.spiel;

public abstract class Spielobjekt {

    private Spieler spieler;

    public Spielobjekt(Spieler spieler) {
        this.spieler = spieler;
    }

    public Spieler getSpieler() {
        return spieler;
    }

    public abstract void benutzen();
}
