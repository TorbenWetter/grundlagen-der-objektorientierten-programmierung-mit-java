package org.iu.dsgopj102101.uebungsprojekt.xrohr;

/**
 * Repraesentiert ein Rohr, das aus einem Durchmesser, einer Dicke und einer Laenge besteht.
 *
 * @author Torben Wetter
 */
public class Rohr {

    private float d1;
    private float l;
    private float s;

    public Rohr(float d1, float l, float s) {
        this.d1 = d1;
        this.l = l;
        this.s = s;
    }

    /**
     * Berechnet das Volumen des Rohres.
     *
     * @return Volumen des Rohres
     */
    public float berechneVolumen() {
        return (float) (Math.PI * Math.pow(this.d1 / 2, 2) * this.l);
    }
}
