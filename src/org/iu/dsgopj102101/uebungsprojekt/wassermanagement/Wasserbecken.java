package org.iu.dsgopj102101.uebungsprojekt.wassermanagement;

public abstract class Wasserbecken {

    /**
     * Berechnet das Volumen
     *
     * @return float (Volumen in Kubikmeter)
     */
    public abstract float volumen();

    /**
     * Berechnet die Wassermenge
     *
     * @return float (Wassermenge in Liter)
     */
    public float wassermenge() {
        return volumen() * 1000;
    }

}
