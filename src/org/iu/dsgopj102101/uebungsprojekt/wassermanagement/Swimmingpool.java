package org.iu.dsgopj102101.uebungsprojekt.wassermanagement;

public class Swimmingpool extends Wasserbecken {

    private float laenge;
    private float breite;
    private float hoehe;

    public Swimmingpool(float laenge, float breite, float hoehe) {
        this.laenge = laenge;
        this.breite = breite;
        this.hoehe = hoehe;
    }

    @Override
    public float volumen() {
        return this.laenge * this.breite * this.hoehe;
    }

}
