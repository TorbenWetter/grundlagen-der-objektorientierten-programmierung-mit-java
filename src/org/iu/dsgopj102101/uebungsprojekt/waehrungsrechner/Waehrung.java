package org.iu.dsgopj102101.uebungsprojekt.waehrungsrechner;

public enum Waehrung {

    EURO("Euro", "EUR", "€", 1.0),
    US_DOLLAR("US-Dollar", "USD", "$", 0.872),
    SCHWEIZER_FRANKEN("Schweizer Franken", "CHF", "Fr.", 0.9463),
    TAKA("Bangladeschischer Taka", "BDT", "৳", 0.010435);

    private final String name;
    private final String code;
    private final String symbol;
    private final double euroBetrag;

    Waehrung(String name, String code, String symbol, double euroBetrag) {
        this.name = name;
        this.code = code;
        this.symbol = symbol;
        this.euroBetrag = euroBetrag;
    }

    public String getName() {
        return name;
    }

    public String getSymbol() {
        return symbol;
    }

    public double getEuroBetrag() {
        return euroBetrag;
    }

    /**
     * Setzt einen informativen String für die Währung zusammen.
     * Wenn es sich nicht um den Euro handelt, wird der äquivalente Euro-Betrag ausgegeben.
     *
     * @return String mit Währungsinformationen
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.name);
        sb.append(" (Symbol: ");
        sb.append(this.symbol);
        if (this != EURO) {
            sb.append(", Euro-Wert: ");
            sb.append(String.format("%.2f €", euroBetrag));
        }
        sb.append(")");
        return sb.toString();
    }

    /**
     * Gibt die Währung mit dem angegebenen ISO-Code zurück.
     *
     * @param code ISO-4217-Code der Währung.
     * @return Währung mit dem angegebenen ISO-Code.
     * @throws IllegalArgumentException wenn die Währung nicht existiert.
     */
    public static Waehrung getWaehrung(String code) throws IllegalArgumentException {
        for (Waehrung waehrung : Waehrung.values()) {
            if (waehrung.code.equals(code)) {
                return waehrung;
            }
        }
        throw new IllegalArgumentException("Ungültige Währung mit Code: " + code);
    }
}
