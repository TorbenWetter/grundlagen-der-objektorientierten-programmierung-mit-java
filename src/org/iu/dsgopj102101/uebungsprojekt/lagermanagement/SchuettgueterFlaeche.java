package org.iu.dsgopj102101.uebungsprojekt.lagermanagement;

import java.util.Stack;

public class SchuettgueterFlaeche<E> extends Stack<E> implements Lagerplatz<E> {

    @Override
    public void einlagern(E e) {
        this.push(e);
    }

}
