package org.iu.dsgopj102101.uebungsprojekt.lagermanagement;

public interface Lagerplatz<E> {

    public void einlagern(E e);
}
