package org.iu.dsgopj102101.uebungsprojekt;

import org.iu.dsgopj102101.uebungsprojekt.blackjack.Spiel;

import java.util.Scanner;

public class BlackJack {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Wie viele Spieler sollen gegen die Maschine spielen? ");
        int anzahlMenschen = scanner.nextInt();
        Spiel spiel = new Spiel(anzahlMenschen);
        spiel.spielen();
    }
}
